# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score




path_train = "image_train_data.csv"
path_test = 'image_test_data.csv'

df_train = pd.read_csv(path_train)
df_test = pd.read_csv(path_test)
#df_train['label'].value_counts().index[-1]

def compute_features(value):
    features = value\
        .replace('[','').replace(']','').split(' ')
    return pd.Series(features).astype(float)
        
df_feats = df_train['deep_features'].apply(compute_features)
df_train = df_train.join(df_feats)
df_feats = df_test['deep_features'].apply(compute_features)
df_test = df_test.join(df_feats)
#df_img = df_train['image_array'].apply(compute_features)
#df_train = df_train.join(df_img)



'''
import PIL
# img
data_aux = df_img.loc[0].astype(int).values
data_aux1 = data_aux  / 30
data = data_aux1.copy()
data = data.reshape(32,32,3)
img = PIL.Image.fromarray(data,'RGB')
img.show()
'''







columns = df_feats.columns
target = 'label'

'''
# dog
df_train_dog = df_train[df_train['label'] == 'dog'] 
dog_model = KNeighborsClassifier()
dog_model.fit(df_train_dog[columns],df_train_dog[target])
selected = df_test.loc[0,columns]
neighbors = dog_model.kneighbors(selected.values.reshape(1,-1),5)
neighbors[1].mean()


# cat
df_train_cat = df_train[df_train['label'] == 'cat']
cat_model = KNeighborsClassifier()
cat_model.fit(df_train_cat[columns],df_train_cat[target])
selected = df_test.loc[0,columns]
neighbors = cat_model.kneighbors(selected.values.reshape(1,-1),5)
neighbors[1].mean()
'''

'''
df_train_dog = df_train[df_train['label'] == 'dog'] 
dog_model = KNeighborsClassifier(n_neighbors=1)
dog_model.fit(df_train_dog[columns],df_train_dog[target])
df_test_dog = df_test[df_test['label'] == 'dog'] 
y_true = df_test_dog['label']
y_pred = dog_model.predict(df_test_dog[columns])
accuracy_score(y_true, y_pred)
'''