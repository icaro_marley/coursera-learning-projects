# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import nltk

path = "amazon_baby.csv"

df = pd.read_csv(path)


df.dropna(inplace=True)


txt = df['review'].str.lower().str.cat(sep=' ')

to_replace = [',','.','/','\\','*','_','+','`',':','~','(',')']

for char in to_replace:
    txt = txt.replace(char,' ')

words = nltk.tokenize.word_tokenize(txt)
word_dist = nltk.FreqDist(words)

selected_words = ['awesome', 'great', 'fantastic', 'amazing', 'love', 'horrible', 'bad', 'terrible', 'awful', 'wow', 'hate']

#selected_words_freq = [word_dist.freq(word) for word in selected_words]
#sorted(zip(selected_words,selected_words_freq),key=lambda tup: tup[1])
#check = df['review'].str.get_dummies(sep=' ')
#df.describe()

df_train, df_test = train_test_split(df,train_size=.8,random_state=0)
df_train = df_train.copy()
df_test = df_test.copy()

# sentiment
df_train = df_train[df_train['rating'] != 3]
df_test = df_test[df_test['rating'] != 3]
# positive
df_train['sentiment'] = (df_train['rating'] >=4).replace({True:1,False:0})
df_test['sentiment'] = (df_test['rating'] >=4).replace({True:1,False:0})
target = 'sentiment'

# features
for word in selected_words:
    df_train[word] = df_train['review'].str.count(word)
    df_test[word] = df_test['review'].str.count(word)


# train
model = LogisticRegression(random_state=0)
X_train = df_train[selected_words]
X_test = df_test[selected_words]
y_train = df_train[target]
y_test = df_test[target]
model.fit(X_train,y_train)


check = pd.DataFrame(model.predict_proba(X_train),columns = ['0','1'])

#X_train.columns[np.where(model.coef_[0] == max(model.coef_[0]))]
#X_train.columns[np.where(model.coef_[0] == min(model.coef_[0]))]

ytrue = y_test
ypred = model.predict(X_test)
accuracy_score(ytrue, ypred)

#y_test.value_counts()
ytrue = y_test
ypred = [1] * len(y_test)
accuracy_score(ytrue, ypred)
# majority
