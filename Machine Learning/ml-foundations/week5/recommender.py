# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.neighbors import KNeighborsClassifier

path = "song_data.csv"

df = pd.read_csv(path)


check = df.groupby('artist')['user_id'].unique().apply(len).sort_values()
check = df.groupby('artist')['listen_count'].sum().sort_values()


