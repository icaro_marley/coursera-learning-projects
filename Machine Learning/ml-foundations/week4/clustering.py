# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.neighbors import KNeighborsClassifier

path = "people_wiki.csv"

df = pd.read_csv(path)


df[df['name']=='Elton John']['text'].values[0]


#df.dropna(inplace=True)

'''
txt = df[df['name']=='Elton John']['text'].values[0]
to_replace = [',','.','/','\\','*','_','+','`',':','~','(',')']
for char in to_replace:
    txt = txt.replace(char,' ')
words = nltk.tokenize.word_tokenize(txt)
word_dist = nltk.FreqDist(words)
word_dist.most_common(5)
'''


corpus = df['text'].values
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)





#doc_tfidf = vectorizer.transform([txt])

'''
# elton john
feature_names = vectorizer.get_feature_names()
doc = df[df['name']=='Elton John'].index
feature_index = X[doc,:].nonzero()[1]
tfidf_scores = zip(feature_index, [X[doc, x] for x in feature_index])
tfidf_scores = sorted(tfidf_scores,key=lambda tup: tup[1])
for w, s in [(feature_names[i], s) for (i, s) in tfidf_scores]:
  print (w, s)
'''

'''
doc1 = df[df['name']=='Elton John'].index
doc1 = X[doc1,:].toarray()
doc2 = df[df['name']=='Victoria Beckham'].index
doc2 = X[doc2,:].toarray()
cosine_similarity(doc1,doc2)
doc3 = df[df['name']=='Paul McCartney'].index
doc3 = X[doc3,:].toarray()
cosine_similarity(doc1,doc3)

vectorizer_counts = CountVectorizer()
X_counts = vectorizer_counts.fit_transform(corpus)
model = KNeighborsClassifier()
model.fit(X_counts,[1] * X_counts.shape[0])
doc = df[df['name']=='Elton John'].index
index = model.kneighbors(X_counts[doc],2)[1][0,1]
df.iloc[index]['name']

doc = df[df['name']=='Victoria Beckham'].index
index = model.kneighbors(X_counts[doc],2)[1][0,1]
df.iloc[index]['name']
'''

'''
corpus = df['text'].values
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)
model = KNeighborsClassifier()
model.fit(X,[1] * X.shape[0])
doc = df[df['name']=='Elton John'].index
index = model.kneighbors(X[doc],2)[1][0,1]
df.iloc[index]['name']

doc = df[df['name']=='Victoria Beckham'].index
index = model.kneighbors(X[doc],2)[1][0,1]
df.iloc[index]['name']
'''