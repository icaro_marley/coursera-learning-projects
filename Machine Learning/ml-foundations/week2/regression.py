# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

path = "home_data.csv"

df = pd.read_csv(path)


df.describe()

len(df[(2000 < df['sqft_living']) & (df['sqft_living'] < 4000)])/len(df)

df.groupby('zipcode')['price'].mean().max()

df_train, df_test = train_test_split(df,train_size=.8,random_state=0)
target = 'price'

model = LinearRegression()
my_features = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode']

X_train = df_train[my_features]
X_test = df_test[my_features]
y_train = df_train[target]
y_test = df_test[target]
model.fit(X_train,y_train)
ytrue = y_test
ypred = model.predict(X_test)
error1 = np.sqrt(mean_squared_error(ytrue, ypred))




model = LinearRegression()
advanced_features = [
'bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode',
'condition', # condition of house				
'grade', # measure of quality of construction				
'waterfront', # waterfront property				
'view', # type of view				
'sqft_above', # square feet above ground				
'sqft_basement', # square feet in basement				
'yr_built', # the year built				
'yr_renovated', # the year renovated				
'lat', 'long', # the lat-long of the parcel				
'sqft_living15', # average sq.ft. of 15 nearest neighbors 				
'sqft_lot15', # average lot size of 15 nearest neighbors 
]

X_train = df_train[advanced_features]
X_test = df_test[advanced_features]
y_train = df_train[target]
y_test = df_test[target]
model.fit(X_train,y_train)
ytrue = y_test
ypred = model.predict(X_test)
error2 = np.sqrt(mean_squared_error(ytrue, ypred))





error1 - error2