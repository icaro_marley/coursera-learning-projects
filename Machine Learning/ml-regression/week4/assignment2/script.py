# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn import linear_model


dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':str, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}


sales = pd.read_csv('kc_house_data.csv', dtype=dtype_dict)

def get_numpy_data(df, features, output):
    df['constant'] = 1 
    features = ['constant'] + features
    # select the columns of data_SFrame given by the ‘features’ list into the SFrame ‘features_sframe’
    df_features = df[features]
    # this will convert the features_sframe into a numpy matrix with GraphLab Create >= 1.7!!
    features_matrix = df_features.values
    # assign the column of data_sframe associated with the target to the variable ‘output_sarray’
    df_output = df[output]
    # this will convert the SArray into a numpy array:
    output_array = df_output.values # GraphLab Create>= 1.7!!
    return(features_matrix, output_array)
    
    
def predict_outcome(feature_matrix, weights):
    predictions = np.dot(feature_matrix,weights)
    return(predictions)
    
def feature_derivative_ridge(errors, feature, weight, l2_penalty, feature_is_constant):
    derivative = 0
    if feature_is_constant:
        derivative = 2 * np.dot(errors,feature)
    else:
        derivative = 2 * np.dot(errors,feature) + 2 * l2_penalty * weight
    return derivative


  
'''
(example_features, example_output) = get_numpy_data(sales, ['sqft_living'], 'price')
my_weights = np.array([1., 10.])
test_predictions = predict_outcome(example_features, my_weights)
errors = test_predictions - example_output # prediction errors

# next two lines should print the same values
print(feature_derivative_ridge(errors, example_features[:,1], my_weights[1], 1, False))
print(np.sum(errors*example_features[:,1])*2+20.)
print()

# next two lines should print the same values
print(feature_derivative_ridge(errors, example_features[:,0], my_weights[0], 1, True))
print(np.sum(errors)*2.)

'''

def ridge_regression_gradient_descent(feature_matrix, output, 
        initial_weights, step_size, l2_penalty, max_iterations):
    weights = np.array(initial_weights)
    n_iter = 0
    while n_iter < max_iterations:
        # compute the predictions based on feature_matrix and weights:
        predictions = predict_outcome(feature_matrix,weights)
        # compute the errors as predictions - output:
        errors = predictions - output

        # while not converged, update each weight individually:
        for i in range(len(weights)):
            # compute the derivative for weight[i]:
            isconstant = i == 0
            derivative = feature_derivative_ridge(errors,
                feature_matrix[:, i],weights[i], l2_penalty, isconstant)    
            # update the weight based on step size and derivative:
            #weights[i] -= step_size * derivative
            weights = weights - (step_size * derivative)
        n_iter += 1
    return weights


train_data = pd.read_csv('kc_house_train_data.csv', dtype=dtype_dict)
test_data = pd.read_csv('kc_house_test_data.csv', dtype=dtype_dict)

simple_features = ['sqft_living']
my_output = 'price'
(simple_feature_matrix, output) = get_numpy_data(train_data, simple_features, my_output)
(simple_test_feature_matrix, test_output) = get_numpy_data(test_data, simple_features, my_output)

step_size = 1e-12
max_iterations = 1000
initial_weights = [0] * simple_feature_matrix.shape[1]
l2_penalty = 0

simple_weights_0_penalty = ridge_regression_gradient_descent(
        simple_feature_matrix, output, initial_weights, step_size, l2_penalty, max_iterations)


l2_penalty = 1e11 
simple_weights_high_penalty = ridge_regression_gradient_descent(
        simple_feature_matrix, output, initial_weights, step_size, l2_penalty, max_iterations)


'''
plt.plot(simple_feature_matrix,output,'k.',
        simple_feature_matrix,predict_outcome(simple_feature_matrix, simple_weights_0_penalty),'b-',
        simple_feature_matrix,predict_outcome(simple_feature_matrix, simple_weights_high_penalty),'r-')
'''


# rss
ytrue = test_data[my_output]
preds =  predict_outcome(simple_test_feature_matrix, simple_weights_high_penalty)
errors = preds - ytrue
rss = np.sum(np.square(errors))
rss





model_features = ['sqft_living', 'sqft_living15']
my_output = 'price'
(feature_matrix, output) = get_numpy_data(train_data, model_features, my_output)
(test_feature_matrix, test_output) = get_numpy_data(test_data, model_features, my_output)

l2_penalty = 0
initial_weights = [0] * feature_matrix.shape[1]
step_size = 1e-12
max_iterations = 1000


multiple_weights_0_penalty = ridge_regression_gradient_descent(
        feature_matrix, output, initial_weights, step_size, l2_penalty, max_iterations)



l2_penalty = 1e11 
multiple_weights_high_penalty = ridge_regression_gradient_descent(
        feature_matrix, output, initial_weights, step_size, l2_penalty, max_iterations)



# rss
ytrue = test_data[my_output]
preds =  predict_outcome(test_feature_matrix, multiple_weights_0_penalty)
errors = preds - ytrue
rss = np.sum(np.square(errors))
rss


predict_outcome(test_feature_matrix[0,:], multiple_weights_0_penalty)
predict_outcome(test_feature_matrix[0,:], multiple_weights_high_penalty)


test_data.head(1)['price']


'''
262.9
124.6 
Line fit with no regularization (l2_penalty=0)
Between 5e14 and 8e14 << questoes mudando
172.0 x -> 265.0 x 123.5  137.2
90.0
Between 4e14 and 8e14 x -> Between 2e14 and 4e14 << questoes mudando
The weights learned with high regularization (l2_penalty=1e11)
'''