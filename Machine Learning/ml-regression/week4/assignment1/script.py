# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn import linear_model

def polynomial_dataframe(feature, degree): # feature is pandas.Series type
    # assume that degree >= 1
    # initialize the dataframe:
    poly_dataframe = pd.DataFrame()
    # and set poly_dataframe['power_1'] equal to the passed feature
    poly_dataframe['power_1']  = feature
    # first check if degree > 1
    if degree > 1:
        # then loop over the remaining degrees:
        for power in range(2, degree+1):
            # first we'll give the column a name:
            name = 'power_' + str(power)
            # assign poly_dataframe[name] to be feature^power; use apply(*)
            poly_dataframe[name] = feature ** power
    return poly_dataframe


dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':float, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}

sales = pd.read_csv('kc_house_data.csv', dtype=dtype_dict)
sales = sales.sort_values(['sqft_living','price'])

l2_small_penalty = 1.5e-5


poly15_data = polynomial_dataframe(sales['sqft_living'], 15) # use equivalent of `polynomial_sframe`
model = linear_model.Ridge(alpha=l2_small_penalty, normalize=True)
model.fit(poly15_data, sales['price'])


#model.coef_[0]
'''
#l2_penalty=1e-9 
l2_penalty=1.23e2


largest_coef = -np.inf
smallest_coef = np.inf
for i in range(1,5):
    sales  = pd.read_csv('wk3_kc_house_set_'+str(i)+'_data.csv',dtype=dtype_dict)
    sales = sales.sort_values(['sqft_living','price'])
    
    poly_data = polynomial_dataframe(sales['sqft_living'], 15) 
    columns = poly_data.columns
    poly_data['price'] = sales['price']
    
    target = 'price'
    
    model = linear_model.Ridge(alpha=l2_penalty, normalize=True)
    model.fit(poly_data[columns], sales['price'])

    plt.plot(poly_data['power_1'],poly_data['price'],'.',
    poly_data['power_1'], model.predict(poly_data[columns]),'-')
    plt.title('set'+str(i))
    plt.show()
    if model.coef_[0] < smallest_coef:
        smallest_coef = model.coef_[0]
    if model.coef_[0] > largest_coef:
        largest_coef = model.coef_[0]  
smallest_coef dtype=dtype_dict)
largest_coef
'''






train_valid_shuffled = pd.read_csv('wk3_kc_house_train_valid_shuffled.csv', dtype=dtype_dict)
test = pd.read_csv('wk3_kc_house_test_data.csv', dtype=dtype_dict)


n = len(train_valid_shuffled)
k = 10 # 10-fold cross-validation
target = 'price'

poly_data = polynomial_dataframe(train_valid_shuffled['sqft_living'], 15) 
columns = poly_data.columns
target = 'price'
poly_data['price'] = train_valid_shuffled['price']

def k_fold_cross_validation(k, l2_penalty, data, output):
    rss_list = []
    for i in range(k):
        start = (n*i)//k
        end = (n*(i+1))//k
        #print(start,end)
        valid_X = data[start:end+1]    
        train_X = data[0:start].append(data[end+1:n])
        
        valid_y = output[start:end+1]    
        train_y = output[0:start].append(output[end+1:n])

        model = linear_model.Ridge(alpha=l2_penalty, normalize=True)
        model.fit(train_X, train_y)
    
        ytrue = valid_y
        preds =  model.predict(valid_X)
        errors = preds - ytrue
        rss = np.sum(np.square(errors))
        rss_list.append(rss)
        # rss on valid
    result = np.mean(rss_list)   
    return result


best_rss = np.inf
l2 = None
for l2_pen in np.logspace(3, 9, num=13):
    
    rss = k_fold_cross_validation(k,l2_pen,poly_data[columns],poly_data[target])
    # rss
    if rss < best_rss:
        best_rss = rss
        l2 = l2_pen
l2



model = linear_model.Ridge(alpha=l2, normalize=True)

model.fit(poly_data[columns],poly_data[target])


# rss
poly_data = polynomial_dataframe(test['sqft_living'], 15) 
poly_data['price'] = test['price']


ytrue = poly_data[target]
preds =  model.predict(poly_data[columns])
errors = preds - ytrue
rss = np.sum(np.square(errors))