# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt


def polynomial_dataframe(feature, degree): # feature is pandas.Series type
    # assume that degree >= 1
    # initialize the dataframe:
    poly_dataframe = pd.DataFrame()
    # and set poly_dataframe['power_1'] equal to the passed feature
    poly_dataframe['power_1']  = feature
    # first check if degree > 1
    if degree > 1:
        # then loop over the remaining degrees:
        for power in range(2, degree+1):
            # first we'll give the column a name:
            name = 'power_' + str(power)
            # assign poly_dataframe[name] to be feature^power; use apply(*)
            poly_dataframe[name] = feature ** power
    return poly_dataframe


dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':str, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}

'''
sales  = pd.read_csv('kc_house_data.csv',dtype=dtype_dict)
sales = sales.sort_values(['sqft_living','price'])

poly1_data = polynomial_dataframe(sales['sqft_living'], 15) # 2 3 15
columns = poly1_data.columns
poly1_data['price'] = sales['price']



poly1_data
target = 'price'

model1 = LinearRegression().fit(poly1_data[columns],poly1_data[target])



plt.plot(poly1_data['power_1'],poly1_data['price'],'.',
poly1_data['power_1'], model1.predict(poly1_data[columns]),'-')
'''
'''
#Estimate a 15th degree polynomial on all 4 sets, plot the results and view the coefficients for all four models.
# set 1
for i in range(1,5):
    sales  = pd.read_csv('wk3_kc_house_set_'+str(i)+'_data.csv',dtype=dtype_dict)
    sales = sales.sort_values(['sqft_living','price'])
    
    poly1_data = polynomial_dataframe(sales['sqft_living'], 15) 
    columns = poly1_data.columns
    poly1_data['price'] = sales['price']
    
    
    
    poly1_data
    target = 'price'
    
    model1 = LinearRegression().fit(poly1_data[columns],poly1_data[target])
    
    plt.plot(poly1_data['power_1'],poly1_data['price'],'.',
    poly1_data['power_1'], model1.predict(poly1_data[columns]),'-')
    plt.title('set'+str(i))
    plt.show()
    print(model1.coef_[-1])
'''    
    

rss_list = []
for i in range(1,16):
    sales  = pd.read_csv('wk3_kc_house_train_data.csv',dtype=dtype_dict)
    sales = sales.sort_values(['sqft_living','price'])
    
    poly1_data = polynomial_dataframe(sales['sqft_living'], i) 
    columns = poly1_data.columns
    poly1_data['price'] = sales['price']
    target = 'price'
    
    model = LinearRegression().fit(poly1_data[columns],poly1_data[target])
    model.predict(poly1_data[columns])


    sales = pd.read_csv('wk3_kc_house_valid_data.csv',dtype=dtype_dict)
    
    poly1_data = polynomial_dataframe(sales['sqft_living'], i) 
    poly1_data['price'] = sales['price']

    output = poly1_data['price']
    preds =  model.predict(poly1_data[columns])
    errors = preds - output
    rss = np.sum(np.square(errors))
    rss_list.append(rss)

print(np.argsort(rss_list)[0] + 1)


degree = np.argsort(rss_list)[0] + 1


sales  = pd.read_csv('wk3_kc_house_train_data.csv',dtype=dtype_dict)
sales = sales.sort_values(['sqft_living','price'])

poly1_data = polynomial_dataframe(sales['sqft_living'], degree) 
columns = poly1_data.columns
poly1_data['price'] = sales['price']
target = 'price'

model = LinearRegression().fit(poly1_data[columns],poly1_data[target])
model.predict(poly1_data[columns])


sales = pd.read_csv('wk3_kc_house_test_data.csv',dtype=dtype_dict)

poly1_data = polynomial_dataframe(sales['sqft_living'], degree) 
poly1_data['price'] = sales['price']

output = poly1_data['price']
preds =  model.predict(poly1_data[columns])
errors = preds - output
rss = np.sum(np.square(errors))
print(rss)
