# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':str, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}

df_train  = pd.read_csv('kc_house_train_data.csv',dtype=dtype_dict)

df_test  = pd.read_csv('kc_house_test_data.csv',dtype=dtype_dict)

def get_numpy_data(df, features, output):
    df['constant'] = 1 
    features = ['constant'] + features
    # select the columns of data_SFrame given by the ‘features’ list into the SFrame ‘features_sframe’
    df_features = df[features]
    # this will convert the features_sframe into a numpy matrix with GraphLab Create >= 1.7!!
    features_matrix = df_features.values
    # assign the column of data_sframe associated with the target to the variable ‘output_sarray’
    df_output = df[output]
    # this will convert the SArray into a numpy array:
    output_array = df_output.values # GraphLab Create>= 1.7!!
    return(features_matrix, output_array)

def predict_outcome(feature_matrix, weights):
    predictions = np.dot(feature_matrix,weights)
    return(predictions)
    
    
def feature_derivative(errors, feature):
    derivative = 2 * np.dot(errors,feature)
    return(derivative)  

def regression_gradient_descent(feature_matrix, output, initial_weights, step_size, tolerance):
    converged = False
    weights = np.array(initial_weights)
    while not converged:
        # compute the predictions based on feature_matrix and weights:
        predictions = predict_outcome(feature_matrix,weights)
        # compute the errors as predictions - output:
        errors = predictions - output

        
        gradient_sum_squares = 0 # initialize the gradient
        # while not converged, update each weight individually:
        for i in range(len(weights)):
            # compute the derivative for weight[i]:
            derivative = feature_derivative(errors,feature_matrix[:, i])    
            # add the squared derivative to the gradient magnitude
            gradient_sum_squares += np.square(derivative)
            # update the weight based on step size and derivative:
            weights[i] -= step_size * derivative
        gradient_magnitude = np.sqrt(gradient_sum_squares)
        if gradient_magnitude < tolerance:
            converged = True
    return weights
    
#'''
model_features = ['sqft_living']
my_output= 'price'
(feature_matrix, output) = get_numpy_data(df_train, model_features, my_output)
initial_weights = np.array([-47000., 1.])
step_size = 7e-12
tolerance = 2.5e7
#'''
'''
model_features = ['sqft_living', 'sqft_living15']
my_output = 'price'
(feature_matrix, output) = get_numpy_data(df_train, model_features,my_output)
initial_weights = np.array([-100000., 1., 1.])
step_size = 4e-12
tolerance = 1e9
'''
simple_weights = regression_gradient_descent(feature_matrix, output,initial_weights, step_size, tolerance)

#round(simple_weights[1],1)


def get_regression_predictions(input_feature, intercept, *slope_list):
    print(intercept)
    result = intercept
    for index, slope in enumerate(slope_list):
        result+= input_feature[input_feature.columns[index]] * slope
    return result



int(get_regression_predictions(df_test[model_features],*simple_weights).iloc[0])



def get_regression_predictions2(input_feature, intercept, *slope_list):
    result = intercept
    for index, slope in enumerate(slope_list):
        result+= input_feature[:,index+1] * slope
    return result

test_simple_feature_matrix, test_output = get_numpy_data(df_test, \
        model_features, my_output)
predicted_price = get_regression_predictions2(test_simple_feature_matrix,-46999.88716555, 281.91211912)
int(predicted_price[0])






simple_weights
#rss 
preds =  get_regression_predictions(df_test[model_features], *simple_weights).values
#preds = preds[:,0]
errors = preds - output
np.sum(np.square(errors))
# 1201918392690275.5 > 1186978095794202.5


df_test[my_output].iloc[0]