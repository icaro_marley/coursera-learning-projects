import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression

dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':str, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}

df_train  = pd.read_csv('kc_house_train_data.csv',dtype=dtype_dict)

df_test  = pd.read_csv('kc_house_test.data.csv',dtype=dtype_dict)


# features
df_train['bedrooms_squared'] = df_train['bedrooms'] * df_train['bedrooms']
df_train['bed_bath_rooms'] = df_train['bedrooms'] * df_train['bathrooms']
df_train['log_sqft_living'] = df_train['sqft_living'].apply(np.log)
df_train['lat_plus_long'] = df_train['lat'] + df_train['long']

df_test['bedrooms_squared'] = df_test['bedrooms'] * df_test['bedrooms']
df_test['bed_bath_rooms'] = df_test['bedrooms'] * df_test['bathrooms']
df_test['log_sqft_living'] = df_test['sqft_living'].apply(np.log)
df_test['lat_plus_long'] = df_test['lat'] + df_test['long']


df_test[['bedrooms_squared','bed_bath_rooms','log_sqft_living','lat_plus_long']].describe()

round(df_test['bedrooms_squared'].mean(),2)


# linear regression
target = 'price'

columns1 = ['sqft_living', 'bedrooms', 'bathrooms', 'lat', 'long']
model1 = LinearRegression().fit(df_train[columns1],df_train[target])

columns2 = ['sqft_living', 'bedrooms', 'bathrooms', 'lat','long', 'bed_bath_rooms']
model2 = LinearRegression().fit(df_train[columns2],df_train[target])

columns3 = ['sqft_living', 'bedrooms', 'bathrooms', 'lat','long', 'bed_bath_rooms',
            'bedrooms_squared', 'log_sqft_living', 'lat_plus_long']
model3 = LinearRegression().fit(df_train[columns3],df_train[target])


model1.coef_
model2.coef_

# training
rss1 = np.sum(np.square(model1.predict(df_train[columns1]) - df_train[target]))
rss2 = np.sum(np.square(model2.predict(df_train[columns2]) - df_train[target]))
rss3 = np.sum(np.square(model3.predict(df_train[columns3]) - df_train[target]))

rss_list = [rss1,rss2,rss3]
np.argsort(rss_list)[0]
# test
rss1 = np.sum(np.square(model1.predict(df_test[columns1]) - df_test[target]))
rss2 = np.sum(np.square(model2.predict(df_test[columns2]) - df_test[target]))
rss3 = np.sum(np.square(model3.predict(df_test[columns3]) - df_test[target]))

rss_list = [rss1,rss2,rss3]
np.argsort(rss_list)[0]