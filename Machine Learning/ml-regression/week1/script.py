import numpy as np
import pandas as pd

dtype_dict = {'bathrooms':float, 'waterfront':int, 'sqft_above':int, 'sqft_living15':float, 'grade':int, 'yr_renovated':int, 'price':float, 'bedrooms':float, 'zipcode':str, 'long':float, 'sqft_lot15':float, 'sqft_living':float, 'floors':str, 'condition':int, 'lat':float, 'date':str, 'sqft_basement':int, 'yr_built':int, 'id':str, 'sqft_lot':int, 'view':int}


df_train  = pd.read_csv('kc_house_train_data.csv',dtype=dtype_dict)

def simple_linear_regression(input_feature, output):
    X = input_feature
    Y = output
    numerator = np.mean(X * Y) - np.mean(X)*np.mean(Y)
    denominator = np.mean(np.square(X)) - np.mean(X)*np.mean(X)
    slope = numerator / denominator
    intercept = np.mean(Y) - slope * np.mean(X)
    return intercept, slope
    




input_feature = df_train['sqft_living'] # bedrooms
output = df_train['price']
intercept, slope = simple_linear_regression(input_feature, output)



def get_regression_predictions(input_feature, intercept, slope):
    return input_feature * slope + intercept


round(get_regression_predictions(2650,intercept,slope),2)

def get_residual_sum_of_squares(input_feature, output, intercept,slope):
    preds = input_feature.apply(lambda x : get_regression_predictions(x,intercept,slope))
    errors = preds - output
    return np.sum(np.square(errors))

get_residual_sum_of_squares(input_feature,output,intercept,slope)
    

def inverse_regression_predictions(output, intercept, slope):
    return (output - intercept) / slope

inverse_regression_predictions(np.array([800000]),intercept,slope)




df_test  = pd.read_csv('kc_house_test_data.csv',dtype=dtype_dict)


input_feature = df_test['sqft_living'] # bedrooms
get_residual_sum_of_squares(input_feature,output,intercept,slope)
# 931869355290033.9  >  736452059584870.5